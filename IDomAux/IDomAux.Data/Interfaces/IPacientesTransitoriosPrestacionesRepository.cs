﻿using AppService.Entities;

namespace IDomAux.Data
{
    public interface IPacientesTransitoriosPrestacionesRepository : IGenericRepository<pacientes_transitorios_prestaciones>
    {
    }
}
