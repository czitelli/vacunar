﻿using AppService.Entities;

namespace IDomAux.Data
{
    public interface IPacientesTransitoriosRepository : IGenericRepository<pacientes_transitorios>
    {
    }
}
