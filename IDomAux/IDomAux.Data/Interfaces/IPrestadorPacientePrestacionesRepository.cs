﻿using AppService.Entities;

namespace IDomAux.Data
{
    public interface IPrestadorPacientePrestacionesRepository : IGenericRepository<prestador_paciente_prestaciones>
    {
    }
}
