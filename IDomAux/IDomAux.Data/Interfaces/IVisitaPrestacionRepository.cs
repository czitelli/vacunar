﻿using AppService.Entities;

namespace IDomAux.Data
{
    public interface IVisitaPrestacionRepository : IGenericRepository<visita_prestacion>
    {
    }
}
