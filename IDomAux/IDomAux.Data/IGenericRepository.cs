﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IDomAux.Data
{
    /// <summary>
    /// Provee un simple set de metodos para interactuar con un repositorio 
    /// </summary>
    /// <typeparam name="T">T es una entidad de IDomAuxEntities</typeparam>
    public interface IGenericRepository<T> 
        where T : class
    {
        IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);
        IList<T> GetList(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties);
        IList<T> GetList(WhereIfParam<T>[] whereIfs, params Expression<Func<T, object>>[] navigationProperties);
        T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties);
        T Add(params T[] items);
        void Update(params T[] items);
        void Remove(params T[] items);
    }
}
