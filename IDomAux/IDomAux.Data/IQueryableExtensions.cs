﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IDomAux.Data
{
    /// <summary>
    /// WhereIf parameter: el predicado a incluir si se cumple la condicion
    /// </summary>
    public class WhereIfParam<TSource>
    {
        public bool condition;
        public Expression<Func<TSource, bool>> predicate;
    }

    /// <summary>
    /// Metodos de extension para IQueryable<TSource>
    /// </summary>
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Clausula Where condicional. Incluye el predicado de whereIfParam en IQueryable<TSource>, si se cumple la condicion de whereIfParam.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="whereIfParam"></param>
        /// <returns></returns>
        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, WhereIfParam<TSource> whereIfParam)
        {
            if (whereIfParam.condition)
                return source.Where(whereIfParam.predicate);
            else
                return source;
        }

        /// <summary>
        /// Clausula Where condicional. Incluye el predicado de whereIfParam en IEnumerable<TSource>, si se cumple la condicion de whereIfParam.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="whereIfParam"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> WhereIf<TSource>(this IEnumerable<TSource> source, WhereIfParam<TSource> whereIfParam)
        {
            if (whereIfParam.condition)
                return source.Where(whereIfParam.predicate.Compile());
            else
                return source;
        }
    }
}

