﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using AppService.Entities;

namespace IDomAux.Data
{
    /// <summary>
    /// Repositorio generico de entidades IDomAuxEntities.
    /// Responsable de encapsular el acceso a datos.
    /// </summary>
    /// <typeparam name="T">T es una entidad de IDomAuxEntities</typeparam>
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        public AppServiceEntities context;

        public GenericRepository(AppServiceEntities context)
        {
            this.context = context;
        }

        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = context.Set<T>();

            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery.Include<T, object>(navigationProperty);

            list = dbQuery
                .AsNoTracking()
                .ToList<T>();
            return list;
        }

        public virtual IList<T> GetList(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = context.Set<T>();

            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            list = dbQuery
                .AsNoTracking()
                .Where(where)
                .ToList<T>();
            return list;
        }

        public virtual IList<T> GetList(WhereIfParam<T>[] whereIfs, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = context.Set<T>();

            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            foreach (WhereIfParam<T> whereIf in whereIfs)
                dbQuery = dbQuery.WhereIf(whereIf);

            list = dbQuery
                .AsNoTracking()
                .ToList<T>();
            return list;
        }

        public virtual T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            T item = null;
            IQueryable<T> dbQuery = context.Set<T>();

            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            item = dbQuery
                .AsNoTracking()
                .FirstOrDefault(where);
            return item;
        }

        public virtual T Add(params T[] items)
        {
            foreach (T item in items)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
            }
            context.SaveChanges();
            return items.Any() ? items.First() : null;
        }

        public virtual void Update(params T[] items)
        {
            foreach (T item in items)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Modified;
            }
            context.SaveChanges();
        }

        public virtual void Remove(params T[] items)
        {
            foreach (T item in items)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }
            context.SaveChanges();
        }
    }
}