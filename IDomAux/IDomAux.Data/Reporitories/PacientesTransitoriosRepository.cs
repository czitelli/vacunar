﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PacientesTransitoriosRepository : GenericRepository<pacientes_transitorios>, IPacientesTransitoriosRepository
    {
        public PacientesTransitoriosRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}