﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PacienteRepository : GenericRepository<pacientes>, IPacienteRepository
    {
        public PacienteRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}