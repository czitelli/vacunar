﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PrestadorPacientePrestacionesRepository : GenericRepository<prestador_paciente_prestaciones>, IPrestadorPacientePrestacionesRepository
    {
        public PrestadorPacientePrestacionesRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}