﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class VisitaPrestacionRepository : GenericRepository<visita_prestacion>, IVisitaPrestacionRepository
    {
        public VisitaPrestacionRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}