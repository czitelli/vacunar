﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PrestadorDerivadoRepository : GenericRepository<prestadores_derivados>, IPrestadorDerivadoRepository
    {
        public PrestadorDerivadoRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}