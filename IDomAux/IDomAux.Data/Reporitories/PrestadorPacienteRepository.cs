﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PrestadorPacienteRepository : GenericRepository<prestador_pacientes>, IPrestadorPacienteRepository
    {
        public PrestadorPacienteRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}