﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class VisitaRepository : GenericRepository<prestador_visitas>, IVisitaRepository
    {
        public VisitaRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}