﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PacienteImagenesRepository : GenericRepository<paciente_imagenes>, IPacienteImagenesRepository
    {
        public PacienteImagenesRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}