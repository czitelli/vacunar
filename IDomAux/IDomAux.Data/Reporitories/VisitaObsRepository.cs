﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class VisitaObsRepository : GenericRepository<prestador_visitas_obs>, IVisitaObsRepository
    {
        public VisitaObsRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}