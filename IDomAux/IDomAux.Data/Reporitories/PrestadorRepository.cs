﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PrestadorRepository : GenericRepository<prestadores>, IPrestadorRepository
    {
        public PrestadorRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}