﻿using AppService.Entities;

namespace IDomAux.Data
{
    public class PacientesTransitoriosPrestacionesRepository : GenericRepository<pacientes_transitorios_prestaciones>, IPacientesTransitoriosPrestacionesRepository
    {
        public PacientesTransitoriosPrestacionesRepository(AppServiceEntities context) : base(context)
        {
        }
    }
}