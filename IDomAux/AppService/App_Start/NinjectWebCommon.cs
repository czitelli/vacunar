[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(AppService.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(AppService.App_Start.NinjectWebCommon), "Stop")]

namespace AppService.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using System.Configuration;
    using IDomAux.Core;
    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.WebApi;
    using System.Web.Http;
    using AutoMapper;
    using AutoMapper.Configuration;
    using AppService.Models;
    using AppService.Entities;
    using System.Collections.Generic;
    using IDomAux.Data;
    using System.Data.Entity;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();
        private static StandardKernel _kernel;

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static T GetInstance<T>()
        {
            return _kernel.Get<T>();
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                _kernel = kernel;
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<AppServiceEntities>().To<AppServiceEntities>().InThreadScope().WithConstructorArgument("conn", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            kernel.Bind<IVisitaRepository>().To<VisitaRepository>();
            kernel.Bind<IPrestadorRepository>().To<PrestadorRepository>();
            kernel.Bind<IPrestadorPacienteRepository>().To<PrestadorPacienteRepository>();
            kernel.Bind<IPacienteRepository>().To<PacienteRepository>();
            kernel.Bind<IVisitaObsRepository>().To<VisitaObsRepository>();
            kernel.Bind<IVisitaPrestacionRepository>().To<VisitaPrestacionRepository>();
            kernel.Bind<IPacientesTransitoriosRepository>().To<PacientesTransitoriosRepository>();
            kernel.Bind<IPrestadorPacientePrestacionesRepository>().To<PrestadorPacientePrestacionesRepository>();
            kernel.Bind<IPacientesTransitoriosPrestacionesRepository>().To<PacientesTransitoriosPrestacionesRepository>();
            kernel.Bind<IPrestadorDerivadoRepository>().To<PrestadorDerivadoRepository>();
            kernel.Bind<IPacienteImagenesRepository>().To<PacienteImagenesRepository>();

            var config = new AutoMapper.MapperConfiguration(ConfigureMapper);
            kernel.Bind<IConfigurationProvider>().ToConstant<MapperConfiguration>(config);
            kernel.Bind<IMapper>().To<Mapper>();
            kernel.Bind<IDomAuxCore>().To<DomAuxCore>();
        }

        private static void ConfigureMapper(IMapperConfigurationExpression expression)
        {
            expression.CreateMap<Patient, pacientes>()
                    .ForMember(t => t.domicilio_geo_lat, opt => opt.MapFrom(i => decimal.Parse(i.geo != null ? i.geo.latitud : "0")))
                    .ForMember(t => t.domicilio_geo_long, opt => opt.MapFrom(i => decimal.Parse(i.geo != null ? i.geo.longitud : "0")));

            expression.CreateMap<prestadores, Prestador>()
                .ForMember(t => t.id, opt => opt.MapFrom(i => i.prestador_id))
                .ForMember(t => t.email, opt => opt.MapFrom(i => i.email))
                .ForMember(t => t.name, opt => opt.MapFrom(i => i.nombre))
                .ForMember(t => t.prestacion, opt => opt.MapFrom(i => i.tipo))
                .ForMember(t => t.alias, opt => opt.MapFrom(i => i.alias))
                .ForMember(t => t.dir, opt => opt.Ignore())
                .ForMember(t => t.lastname, opt => opt.Ignore())
                .ForMember(t => t.location, opt => opt.Ignore())
                .ForMember(t => t.phone, opt => opt.Ignore());

            expression.CreateMap<prestador_pacientes, PrestadorPaciente>()
                .ForMember(t => t.caso, opt => opt.MapFrom(i => i.caso_id))
                .ForMember(t => t.paciente, opt => opt.MapFrom(i => i.paciente_id))
                .ForMember(t => t.prestador, opt => opt.MapFrom(i => i.prestador_id));

            expression.CreateMap<paciente_imagenes, Image>()
                .ForMember(t => t.fecha_imagen, opt => opt.MapFrom(i => i.fecha_imagen.HasValue ? i.fecha_imagen.Value : i.fecha_alta))
                .ForMember(t => t.paciente_id, opt => opt.MapFrom(i => i.paciente_id))
                .ForMember(t => t.prestador_id, opt => opt.MapFrom(i => i.prestador_id))
                .ForMember(t => t.prestacion_id, opt => opt.MapFrom(i => i.prestacion_id))
                .ForMember(t => t.url_imagen, opt => opt.MapFrom(i => HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + i.url_imagen));

            expression.CreateMap<prestador_paciente_prestaciones, Prestacion>()
                .ForMember(t => t.frecuencia, opt => opt.MapFrom(i => i.periodicidad))
                .ForMember(t => t.imagen, opt => opt.MapFrom(i => i.periodicidad))
                .ForMember(t => t.nombre, opt => opt.MapFrom(i => i.prestacion))
                .ForMember(t => t.imagenes, opt => opt.MapFrom<IList<paciente_imagenes>>(i => i.imagenes));

            expression.CreateMap<pacientes, Patient>()
                .ForMember(t => t.id, opt => opt.MapFrom(i => i.paciente_id))
                .ForMember(t => t.lastname, opt => opt.MapFrom(i => i.apellido))
                .ForMember(t => t.name, opt => opt.MapFrom(i => i.nombre))
                .ForMember(t => t.phone, opt => opt.MapFrom(i => i.telefono))
                .ForMember(t => t.tipo_paciente, opt => opt.MapFrom(i => Patient.EstadoAprobado))
                .ForMember(t => t.derivable, opt => opt.MapFrom(i => i.posible_derivacion))
                .ForMember(t => t.dir, opt => opt.MapFrom(i => i.domicilio))
                .ForMember(t => t.prestaciones, opt => opt.MapFrom<IList<prestador_paciente_prestaciones>>(i => i.prestaciones))
                .ForMember(t => t.Visitas, opt => opt.MapFrom<IList<prestador_visitas>>(i => i.visitas))
                .ForMember(t => t.geo, opt => opt.MapFrom(i => new Location { latitud = i.domicilio_geo_lat.ToString(), longitud = i.domicilio_geo_long.ToString() }));

            expression.CreateMap<pacientes_transitorios, Patient>()
                .ForMember(t => t.id, opt => opt.MapFrom(i => i.id))
                .ForMember(t => t.lastname, opt => opt.MapFrom(i => i.apellido_paciente))
                .ForMember(t => t.name, opt => opt.MapFrom(i => i.nombre_paciente))
                .ForMember(t => t.phone, opt => opt.Ignore())
                .ForMember(t => t.dni, opt => opt.MapFrom(i => i.dni_paciente))
                .ForMember(t => t.dir, opt => opt.MapFrom(i => i.direccion_paciente))
                .ForMember(t => t.tipo_paciente, opt => opt.MapFrom(i => Patient.EstadoPendiente))
                .ForMember(t => t.prestaciones, opt => opt.MapFrom<IList<prestador_paciente_prestaciones>>(i => i.prestaciones))
                .ForMember(t => t.Visitas, opt => opt.MapFrom<IList<prestador_visitas>>(i => i.visitas))
                .ForMember(t => t.geo, opt => opt.Ignore());

            expression.CreateMap<Visit, prestador_visitas>()
                .ForMember(t => t.geo_lat, opt => opt.MapFrom(i => decimal.Parse(i.geo != null ? i.geo.latitud : "0")))
                .ForMember(t => t.fecha, opt => opt.MapFrom(i => i.created_at))
                .ForMember(t => t.visita_id, opt => opt.MapFrom(i => i.id))
                .ForMember(t => t.prestador_id, opt => opt.MapFrom(i => i.id_prestador))
                .ForMember(t => t.geo_long, opt => opt.MapFrom(i => decimal.Parse(i.geo != null ? i.geo.longitud : "0")));

            expression.CreateMap<VisitObs, prestador_visitas_obs>()
                .ForMember(t => t.fecha, opt => opt.MapFrom(i => DateTime.Now))
                .ForMember(t => t.visita_id, opt => opt.MapFrom(i => i.visit_id))
                .ForMember(t => t.observacion, opt => opt.MapFrom(i => i.detail));

            expression.CreateMap<List<prestadores_derivados>, PrestadoresDerivacionesList>()
                .ForMember(t => t.total_items, opt => opt.MapFrom(i => i.Count))
                .ForMember(t => t.derivaciones, opt => opt.MapFrom<List<prestadores_derivados>>(i => i));

            expression.CreateMap<prestadores_derivados, PrestadorDerivaciones>()
                .ForMember(t => t.estado, opt => opt.MapFrom(i => i.estado))
                .ForMember(t => t.derivacion_id, opt => opt.MapFrom(i => i.id))
                .ForMember(t => t.motivo, opt => opt.MapFrom(i => i.motivo_derivacion))
                .ForMember(t => t.prestador_id, opt => opt.MapFrom(i => i.es_emisor ? i.prestador_destino_id : i.prestador_origen_id))
                .ForMember(t => t.tipo_derivacion, opt => opt.MapFrom(i => i.tipo_derivacion))
                .ForMember(t => t.paciente, opt => opt.MapFrom<pacientes>(i => i.paciente))
                .ForMember(t => t.fecha_alta, opt => opt.MapFrom(i => i.fecha));

            expression.CreateMap<AddPrestacion, pacientes_transitorios_prestaciones>()
                .ForMember(t => t.fecha_alta, opt => opt.MapFrom(i => DateTime.Now))
                .ForMember(t => t.paciente_transitorio_id, opt => opt.MapFrom(i => i.paciente_transitorio_id))
                .ForMember(t => t.prestacion_brindada, opt => opt.MapFrom(i => i.prestacion));

            expression.CreateMap<AddPrestadorDerivado, prestadores_derivados>()
                .ForMember(t => t.fecha, opt => opt.MapFrom(i => DateTime.Now))
                .ForMember(t => t.estado, opt => opt.MapFrom(i => EstadoPrestadorDerivado.Pendiente.ToString()))
                .ForMember(t => t.prestador_origen_id, opt => opt.Ignore())
                .ForMember(t => t.motivo_derivacion, opt => opt.MapFrom(i => i.motivo))
                .ForMember(t => t.paciente_id, opt => opt.MapFrom(i => i.paciente_id))
                .ForMember(t => t.prestador_destino_id, opt => opt.MapFrom(i => i.prestador_derivado_id));

            expression.CreateMap<AddVisit, prestador_visitas>()
                .ForMember(t => t.caso_id, opt => opt.MapFrom(i => i.caso_id))
                .ForMember(t => t.fecha, opt => opt.MapFrom(i => i.created_at))
                .ForMember(t => t.geo_lat, opt => opt.MapFrom(i => i.geo.latitud))
                .ForMember(t => t.geo_long, opt => opt.MapFrom(i => i.geo.longitud))
                .ForMember(t => t.prestaciones, opt => opt.Ignore())
                .ForMember(t => t.prestador_id, opt => opt.MapFrom(i => i.id_prestador))
                .ForMember(t => t.visita_id, opt => opt.MapFrom(i => i.id))
                .ForMember(t => t.paciente_transitorio_id, opt => opt.MapFrom(i => i.paciente_transitorio_id))
                .ForMember(t => t.dir, opt => opt.MapFrom(i => i.dir))
                .ForMember(t => t.paciente_id, opt => opt.MapFrom(i => i.id_paciente));

            expression.CreateMap<AddPatient, pacientes_transitorios>()
                .ForMember(t => t.prestador_id, opt => opt.MapFrom(i => i.prestador_id))
                .ForMember(t => t.direccion_paciente, opt => opt.MapFrom(i => i.direccion))
                .ForMember(t => t.apellido_paciente, opt => opt.MapFrom(i => i.apellido))
                .ForMember(t => t.dni_paciente, opt => opt.MapFrom(i => i.dni))
                .ForMember(t => t.fecha_alta, opt => opt.MapFrom(i => DateTime.Now))
                .ForMember(t => t.nombre_paciente, opt => opt.MapFrom(i => i.nombre));

            expression.CreateMap<AddImage, paciente_imagenes>()
                .ForMember(t => t.prestador_id, opt => opt.MapFrom(i => i.prestador_id))
                .ForMember(t => t.paciente_id, opt => opt.MapFrom(i => i.paciente_id))
                .ForMember(t => t.prestacion_id, opt => opt.MapFrom(i => i.prestacion_id))
                .ForMember(t => t.nombre_imagen, opt => opt.MapFrom(i => i.nombre_imagen))
                .ForMember(t => t.fecha_alta, opt => opt.MapFrom(i => DateTime.Now))
                .ForMember(t => t.fecha_imagen, opt => opt.MapFrom(i => i.fecha_imagen));

            expression.CreateMap<prestador_visitas_obs, VisitObs>()
                .ForMember(t => t.created_at, opt => opt.MapFrom(i => i.fecha))
                .ForMember(t => t.detail, opt => opt.MapFrom(i => i.observacion))
                .ForMember(t => t.id, opt => opt.MapFrom(i => i.visita_ln))
                .ForMember(t => t.visit_id, opt => opt.MapFrom(i => i.visita_id));

            expression.CreateMap<VisitaPrestacion, visita_prestacion>()
                .ForMember(t => t.id, opt => opt.MapFrom(i => i.id))
                .ForMember(t => t.visita_id, opt => opt.MapFrom(i => i.visita_id))
                .ForMember(t => t.prestacion_id, opt => opt.MapFrom(i => i.prestacion_id))
                .ForMember(t => t.prestacion_nombre, opt => opt.MapFrom(i => i.prestacion_nombre));

            expression.CreateMap<prestador_visitas, Visit>()
                .ForMember(t => t.id, opt => opt.MapFrom(i => i.visita_id))
                .ForMember(t => t.id_paciente, opt => opt.MapFrom(i => i.caso_id))
                .ForMember(t => t.id_prestador, opt => opt.MapFrom(i => i.prestador_id))
                .ForMember(t => t.created_at, opt => opt.MapFrom(i => i.fecha))
                .ForMember(t => t.update_at, opt => opt.MapFrom(i => i.fecha))
                .ForMember(t => t.geo, opt => opt.MapFrom(i => new Location { latitud = i.geo_lat.ToString(), longitud = i.geo_long.ToString() }));
        }
    }
}