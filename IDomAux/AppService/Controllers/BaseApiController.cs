﻿using IDomAux.Core;
using AppService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IDomAux;
using AppService.Models;
using AutoMapper;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace AppService.Controllers
{
    [Authorize]
    public class BaseApiController : ApiController
    {
        protected readonly IDomAuxCore Core;
        protected readonly IMapper MapperInstance;
        protected readonly int PatientTypeDelimiterId;
        public Prestador Prestador => MapperInstance.Map<Prestador>(Core.GetPrestador(User.Identity.Name));

        public BaseApiController(IMapper mapper, IDomAuxCore core) : base()
        {
            Core = core;
            MapperInstance = mapper;
            PatientTypeDelimiterId = 100000000;
        }

        protected int GetPatientTypeId(int id)
        {
            return id >= PatientTypeDelimiterId ? Patient.EstadoPendiente : Patient.EstadoAprobado;
        }
    }
}
