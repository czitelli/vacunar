﻿using AppService.Entities;
using AppService.Models;
using AutoMapper;
using IDomAux.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AppService.Controllers
{
    [Authorize]
    public class PrestadoresController : BaseApiController
    {
        public PrestadoresController(IMapper mapper, IDomAuxCore core) : base(mapper, core)
        {
        }

        [HttpGet, ResponseType(typeof(PrestadoresList))]
        public IHttpActionResult Get()
        {
            try
            {
                var count = 0;
                var prestadores = MapperInstance.Map<List<Prestador>>(Core.GetAllPrestadores(Prestador.id, out count));
                var model = new PrestadoresList() { total_items = count, Prestadores = prestadores };
                return Ok(model);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, ResponseType(typeof(PrestadoresDerivacionesList)), Route("api/prestadores/derivaciones")]
        public IHttpActionResult Derivaciones()
        {
            try
            {
                var prestadorDerivacionesEmisor = MapperInstance.Map<PrestadoresDerivacionesList>(Core.GetPrestadoresDerivados(Prestador.id));
                return Ok(prestadorDerivacionesEmisor);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/prestadores/derivaciones/{derivacionId}/aprobar")]
        public IHttpActionResult Aprobar(int derivacionId)
        {
            try
            {
                var derivacion = MapperInstance.Map<PrestadorDerivaciones>(Core.GetDerivacion(Prestador.id, derivacionId));
                if(derivacion == null)
                    return BadRequest("La derivacion no esta dirijida al prestador actual o no existe");
                if (derivacion.estado != EstadoPrestadorDerivado.Pendiente.ToString())
                    return BadRequest("La derivacion no se encuentra en estado pendiente");
                var derivacionEntity = Core.ActualizarEstadoDerivacion(Prestador.id, derivacionId, EstadoPrestadorDerivado.AprobadoPrestador.ToString());
                if (derivacionEntity == null)
                {
                    return NotFound();
                }
                Core.AceptarDerivarPaciente(derivacionEntity);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/prestadores/derivaciones/{derivacionId}/cancelar")]
        public IHttpActionResult Cancelar(int derivacionId)
        {
            try
            {
                var derivacion = MapperInstance.Map<PrestadorDerivaciones>(Core.GetDerivacion(Prestador.id, derivacionId));
                if (derivacion == null)
                    return BadRequest("La derivacion no esta dirijida al prestador actual o no existe");
                if (derivacion.estado != EstadoPrestadorDerivado.Pendiente.ToString())
                    return BadRequest("La derivacion no se encuentra en estado pendiente");
                var patient = MapperInstance.Map<PrestadorDerivaciones>(Core.ActualizarEstadoDerivacion(Prestador.id, derivacionId, EstadoPrestadorDerivado.Cancelado.ToString()));
                if (patient == null)
                    return NotFound();
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/prestadores/derivar")]
        public HttpResponseMessage New(AddPrestadorDerivado derivacion)
        {
            try
            {
                var patient = MapperInstance.Map<Patient>(Core.GetPatient(derivacion.paciente_id, Prestador.id));
                if (patient == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Empty);
                string message = string.Empty;
                var prestadorInsertado = Core.AddPrestadorDerivado(MapperInstance.Map<prestadores_derivados>(derivacion), Prestador.id, out message);
                if (prestadorInsertado == null)
                    return Request.CreateErrorResponse(HttpStatusCode.Conflict, message);
                return Request.CreateErrorResponse(HttpStatusCode.OK, message);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
    }
}
