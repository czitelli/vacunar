﻿using AppService.Entities;
using AppService.Models;
using AutoMapper;
using IDomAux.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace AppService.Controllers
{
    [Authorize]
    public class PatientsController : BaseApiController
    {
        public PatientsController(IMapper mapper, IDomAuxCore core) : base(mapper, core)
        {
        }

        public PatientList Get(int? page = 1, int? pagesize = 10)
        {
            page = page > 0 ? page : 1;
            pagesize = pagesize > 0 ? pagesize : 1;
            var totalApproved = 0;
            var approvedList = MapperInstance.Map<List<Patient>>(Core.GetAllPatients(Prestador.id, out totalApproved, page, pagesize));
            var totalUnapproved = 0;
            var unapprovedList = MapperInstance.Map<List<Patient>>(Core.GetAllPendingPatients(Prestador.id, out totalUnapproved, page, pagesize));
            var pacientes = approvedList.Union(unapprovedList).ToList();
            var total = totalApproved + totalUnapproved;
            var model = new PatientList { Patients = pacientes.Skip(((page.Value - 1) * pagesize.Value)).Take(pagesize.Value).ToList() };
            var pages = total % pagesize.Value > 0 ? (total / pagesize.Value) + 1 : total / pagesize.Value;

            model.Pagination = new Pagination() { current_page = page.Value, items_per_page = pagesize.Value, total_items = total, total_pages = pages };
            model.Prestador = Prestador;
            return model;

        }

        [HttpGet, ResponseType(typeof(Patient))]
        public IHttpActionResult Get(int id)
        {
            var patientType = GetPatientTypeId(id);
            Patient patient = null;
            if (patientType == Patient.EstadoAprobado)
                patient = MapperInstance.Map<Patient>(Core.GetPatient(id, Prestador.id));
            else
                if (patientType == Patient.EstadoPendiente)
                    patient = MapperInstance.Map<Patient>(Core.GetPendingPatient(id, Prestador.id));
                else
                    return StatusCode(HttpStatusCode.NoContent);
            if (patient != null)
            {
                var obs = Core.GetAllObservaciones(patient.Visitas.Select(i => i.id).ToList());
                foreach (var visita in patient.Visitas)
                {
                    visita.details = MapperInstance.Map<List<VisitObs>>(obs.Where(o => o.visita_id == visita.id).ToList());
                }

                return Ok<Patient>(patient);
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        //[HttpGet, ResponseType(typeof(Patient))]
        //public IHttpActionResult Get(int id)
        //{
        //    var patient = MapperIntance.Map<Patient>(Core.GetPatient(id, Prestador.id));
        //    if (patient != null)
        //    {
        //        var obs = Core.GetAllObservaciones(patient.Visitas.Select(i => i.id).ToList());
        //        foreach (var visita in patient.Visitas)
        //        {
        //            visita.details = MapperIntance.Map<List<VisitObs>>(obs.Where(o => o.visita_id == visita.id).ToList());
        //        }

        //        return Ok<Patient>(patient);
        //    }
        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        [HttpPost, ResponseType(typeof(Patient))]
        public IHttpActionResult PostPatient(AddPatient patient)
        {
            var patientEntity = MapperInstance.Map<Patient>(Core.GetPatientByDni(patient.dni));
            if (patientEntity == null)
            {
                try
                {
                    patient.prestador_id = Prestador.id;
                    var addedEntity = MapperInstance.Map<Patient>(Core.AddPatient(MapperInstance.Map<pacientes_transitorios>(patient), Prestador.id, patient.prestacion));
                    return Ok(addedEntity);
                }
                catch (Exception e)
                {
                    //System.Web.Http.Results.
                    return InternalServerError(e);
                }
            }
            return Conflict();
        }

        [HttpPost(), Route("api/patients/{patientId}/prestacion/{prestacionId}/images/new"), ]
        public HttpResponseMessage PatientImage(int patientId, int prestacionId)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            var prestacion = MapperInstance.Map<Prestacion>(Core.GetPrestacion(Prestador.id, patientId, prestacionId));
            if (prestacion == null)
            {
                dict.Add("error", string.Format("La prestación o el paciente no pertenece al prestador"));
                return Request.CreateResponse(HttpStatusCode.Conflict, dict);
            }
            var patientImageDto = new AddImage();
            patientImageDto.fecha_imagen = DateTime.Now;
            patientImageDto.prestador_id = Prestador.id;
            patientImageDto.prestacion_id = prestacionId;
            patientImageDto.paciente_id = patientId;
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count == 0)
                {
                    dict.Add("error", string.Format("Por favor, sube una imagen"));
                    return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                }
                foreach (string file in httpRequest.Files)
                {                    
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 10; //Size = 10 MB
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                        var extension = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.')).ToLower();

                        if (!AllowedFileExtensions.Contains(extension))
                            dict.Add("error extension", string.Format("Solo se admiten imagenes del formato .jpg,.png,.jpeg"));
                        else if (postedFile.ContentLength > MaxContentLength)
                            dict.Add("error tamaño maximo", string.Format("Solo se pueden subir imagenes de hasta 10 mb."));
                        else
                        {
                            patientImageDto.nombre_imagen = Path.GetFileName(postedFile.FileName);
                            var patientEntity = MapperInstance.Map<paciente_imagenes>(patientImageDto);
                            if(!Directory.Exists(patientEntity.folder))
                                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(patientEntity.folder));
                            postedFile.SaveAs(HttpContext.Current.Server.MapPath(patientEntity.folder + "/" + patientEntity.nombre_imagen));
                            Core.AddImagen(patientEntity);
                        }
                    }
                }
                return Request.CreateErrorResponse(HttpStatusCode.Created, string.Format("Image Updated Successfully."));
            }
            catch (Exception ex)
            {
                var res = string.Format(ex.Message);
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }
    }
}
