﻿using IDomAux.Core;
using AppService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IDomAux;
using AppService.Models;
using AutoMapper;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace AppService.Controllers
{
    [Authorize]
    public class VisitsController : BaseApiController
    {
        public VisitsController(IMapper mapper, IDomAuxCore core) : base(mapper, core)
        {
        }

        [HttpGet, ResponseType(typeof(VisitList)), Route("api/patients/{patientId}/visits")]
        public IHttpActionResult Get(int patientId, int? page = 1, int? pagesize = 10)
        {
            var patientType = GetPatientTypeId(patientId);
            page = page > 0 ? page : 1;
            pagesize = pagesize > 0 ? pagesize : 1;
            var total = 0;
            var model = BuildModelForVisits(patientId, page.Value, pagesize.Value, patientType, out total);
            if (model == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            return Ok<VisitList>(model);
        }

        [HttpGet, ResponseType(typeof(Visit)), Route("api/patients/{patientId}/visits/{visitId}")]
        public IHttpActionResult Get(int visitId, int patientId)
        {
            var patientType = GetPatientTypeId(patientId);
            Visit visit = MapperInstance.Map<Visit>(Core.GetVisit(visitId, patientId));
            if (visit != null)
            {
                var obs = Core.GetObservaciones(visit.id);
                visit.details = MapperInstance.Map<List<VisitObs>>(obs);
                return Ok<Visit>(visit);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost, ResponseType(typeof(Visit)), Route("api/patients/{patientId}/visits/new")]
        public IHttpActionResult New(int patientId, AddVisit visita)
        {
            var patientType = GetPatientTypeId(patientId);
            if (visita.prestaciones != null && visita.prestaciones.Any())
            {
                visita.update_at = (visita.update_at == DateTime.MinValue) ? visita.created_at : visita.update_at;
                if (patientType == Patient.EstadoPendiente && !visita.paciente_transitorio_id.HasValue)
                {
                    return StatusCode(HttpStatusCode.BadRequest);
                }
                visita.id_prestador = Prestador.id;
                visita.id_paciente = patientId;
                var visitaCreada = MapperInstance.Map<Visit>(Core.AddVisit(MapperInstance.Map<prestador_visitas>(visita), visita.prestaciones));
                if (visitaCreada != null)
                {
                    if (visita.detail != null)
                    {
                        var obs = Core.AddObservacion(new prestador_visitas_obs()
                        {
                            fecha = DateTime.Now,
                            observacion = visita.detail,
                            visita_id = visitaCreada.id
                        });
                        visitaCreada.id_paciente = visita.id_paciente;
                        visitaCreada.details = new List<VisitObs> { MapperInstance.Map<VisitObs>(obs) };
                    }
                    return Ok<Visit>(visitaCreada);
                }
                else
                {
                    return BadRequest("Debe especificar prestaciones validas");
                }
            }
            return BadRequest("Debe especificar prestaciones");
        }

        [HttpPut, ResponseType(typeof(Visit)), Route("api/patients/{patientId}/visits/edit")]
        public IHttpActionResult Edit(int patientId, VisitObs visitaObs)
        {
            var patientType = GetPatientTypeId(patientId);
            var visitaExist = MapperInstance.Map<Visit>(Core.GetVisit(visitaObs.visit_id, patientId));
            if (visitaObs != null && visitaExist != null)
            {
                Core.AddObservacion(MapperInstance.Map<prestador_visitas_obs>(visitaObs));
                var obs = Core.GetObservaciones(visitaExist.id).ToList();
                visitaExist.id_paciente = visitaExist.id_paciente;
                visitaExist.details = MapperInstance.Map<List<VisitObs>>(obs);
                return Ok<Visit>(visitaExist);
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        private VisitList BuildModelForVisits(int patientId, int page, int pageSize, int patientType, out int total)
        {
            total = 0;
            List<prestador_visitas> visits = null;
            if (patientType == Patient.EstadoAprobado)
                visits = Core.GetAllVisits(Prestador.id, page, pageSize, out total, patientId);
            else
                if (patientType == Patient.EstadoPendiente)
                    visits = Core.GetAllPendingVisits(Prestador.id, page, pageSize, out total, patientId);
            if (visits == null)
            {
                return null;
            }
            var items = MapperInstance.Map<List<Visit>>(visits);
            var obs = Core.GetAllObservaciones(items.Select(i => i.id).ToList());
            items.ForEach(i => { i.details = MapperInstance.Map<List<VisitObs>>(obs.Where(o => o.visita_id == i.id).OrderByDescending(o => o.fecha).ToList()); });
            var model = new VisitList
            {
                Prestador = Prestador,
                Visits = items
            };
            var pages = pageSize > 0 ?( total % pageSize > 0 ? (total / pageSize) + 1 : total / pageSize) : 1;
            model.Pagination = new Pagination() { current_page = page, items_per_page = pageSize, total_items = total, total_pages = pages };
            return model;

        }
    }
}
