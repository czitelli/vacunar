﻿using AppService.Entities;
using AppService.Models;
using AutoMapper;
using IDomAux.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AppService.Controllers
{
    [Authorize]
    public class PrestacionesTransitoriasController : BaseApiController
    {
        public PrestacionesTransitoriasController(IMapper mapper, IDomAuxCore core) : base(mapper, core)
        {
        }

        [HttpPost, Route("api/patients/{patientId}/prestaciones/new")]
        public IHttpActionResult New(int patientId, AddPrestacion prestacion)
        {
            var patientEntity = Core.GetPrestacion(prestacion.prestacion, Prestador.id);
            if (patientEntity == null)
            {
                try
                {
                    var addedEntity = Core.AddPrestacion(MapperInstance.Map<pacientes_transitorios_prestaciones>(prestacion), Prestador.id);
                    return Ok();
                }
                catch (Exception e)
                {
                    return InternalServerError(e);
                }
            }
            return Conflict();

        }
    }
}
