﻿using IDomAux.Core;
using AppService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IDomAux;
using AppService.Models;
using AutoMapper;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace AppService.Controllers
{
    [Authorize]
    public class ContactoController : BaseApiController
    {
        public ContactoController(IMapper mapper, IDomAuxCore core) : base(mapper, core)
        {
        }

        [HttpPost]
        public IHttpActionResult Post(string asunto, string cuerpo)
        {
            return Ok();
        }
    }
}
