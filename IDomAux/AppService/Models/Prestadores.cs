﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class PrestadoresList
    {
        public PrestadoresList()
        {

        }

        public int total_items { get; set; }
        public virtual ICollection<Prestador> Prestadores { get; set; }
    }

    public class PrestadoresDerivacionesList
    {
        public PrestadoresDerivacionesList()
        {

        }

        public int total_items { get; set; }
        public virtual ICollection<PrestadorDerivaciones> derivaciones { get; set; }
    }
}
