﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class VisitBase
    {
        public VisitBase()
        {
        }

        public int id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime update_at { get; set; }
        public string dir { get; set; }
        public int id_paciente { get; set; }
        public int id_prestador { get; set; }
        public int caso_id { get; set; }
        public Location geo { get; set; }
        public int? paciente_transitorio_id { get; set; }
    }
    public partial class Visit : VisitBase
    {
        public Visit()
        {
        }

        public virtual ICollection<VisitObs> details { get; set; }
        public virtual ICollection<VisitaPrestacion> prestaciones { get; set; }
    }
}
