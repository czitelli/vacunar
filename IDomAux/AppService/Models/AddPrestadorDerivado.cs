﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class AddPrestadorDerivado
    {
        public string motivo { get; set; }
        public int prestador_derivado_id { get; set; }
        public int paciente_id { get; set; }
    }

    public class PrestadorDerivaciones
    {
        public string tipo_derivacion { get; set; } //  receptor emisor
        public int prestador_id { get; set; }
        public Patient paciente { get; set; }
        public int derivacion_id { get; set; }
        public string motivo { get; set; }
        public DateTime fecha_alta { get; set; }
        public string estado { get; set; }
    }

}
