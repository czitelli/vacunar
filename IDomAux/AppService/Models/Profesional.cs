﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class Prestador
    {
        public Prestador()
        {
        }
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string dir { get; set; }
        public string location { get; set; } // separada por ; latitud, longitud
        public string phone { get; set; }
        public string dni { get; set; }
        public string prestacion { get; set; }
        public string email { get; set; }
        public string alias { get; set; }
    }

}
