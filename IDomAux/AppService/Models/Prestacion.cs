﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class Prestacion
    { 
        public int prestador_id { get; set; }
        public int prestacion_id { get; set; }
        public int paciente_id { get; set; }
        public string nombre { get; set; }
        public string imagen { get; set; }
        public string frecuencia { get; set; }
        public System.DateTime fecha_inicio { get; set; }
        public Nullable<System.DateTime> fecha_fin { get; set; }
        public List<Image> imagenes { get; set; }
    }
}
