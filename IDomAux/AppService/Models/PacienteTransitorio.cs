﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class PacienteTransitorio
    {
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string dir { get; set; }
        public string dni { get; set; }
        public List<Prestaciontransitoria> prestaciones { get; set; }
        public virtual ICollection<Visit> Visitas { get; set; }
    }
}
