﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class Location
    {
        public string latitud { get; set; }
        public string longitud { get; set; }
    }
}
