﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class CambioPrestador
    {
        public int PatientId { get; set; }
        public int OldPrestadorId { get; set; }
        public int NewPrestadorId { get; set; }
    }
}
