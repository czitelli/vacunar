﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class AddVisit : VisitBase
    {
        public string detail { get; set; }
        public int[] prestaciones { get; set; }
    }
}
