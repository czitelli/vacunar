﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class AddPatient
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string direccion { get; set; }
        public string dni { get; set; }
        public string telefono { get; set; }
        public string prestacion { get; set; }
        public int prestador_id { get; set; }
        public int prestador_derivado_id { get; set; }
    }
}
