﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public partial class VisitObs
    {
        public VisitObs()
        {
        }

        public int id { get; set; }
        public DateTime created_at { get; set; }
        public string detail { get; set; }
        public int visit_id { get; set; }
    }
}
