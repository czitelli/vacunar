﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class Patient
    {
        public const int EstadoAprobado = 1;
        public const int EstadoPendiente = 2;
        public const int EstadoDesaprobado = 3;

        public int id { get; set; }
        public int caso_id { get; set; }
        public int os_id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string dir { get; set; }
        public string phone { get; set; }
        public string dni { get; set; }
        public List<Prestacion> prestaciones { get; set; }
        public virtual ICollection<Visit> Visitas { get; set; }
        public Location geo {get;set; }
        public int tipo_paciente { get; set; }
        public int derivable { get; set; }
    }
}
