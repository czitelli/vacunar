﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class Prestaciontransitoria
    { 
        public int prestador_id { get; set; }
        public string nombre { get; set; }
        public System.DateTime fecha_inicio { get; set; }
    }
}
