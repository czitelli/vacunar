﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class Pagination
    {
        public int items_per_page { get; set; }
        public int total_items { get; set; }
        public int current_page { get; set; }
        public int total_pages { get; set; }
    }
}
