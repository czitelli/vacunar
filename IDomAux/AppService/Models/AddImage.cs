﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AppService.Models
{
    public class AddImage
    {
        public DateTime? fecha_imagen { get; set; }
        public int prestador_id { get; set; }
        public int paciente_id { get; set; }
        public int prestacion_id { get; set; }
        public string nombre_imagen { get; set; }
    }
}
