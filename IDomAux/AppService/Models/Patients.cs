﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class PatientList
    {
        public PatientList()
        {

        }

        public Pagination Pagination { get; set; }
        public Prestador Prestador { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }
    }
}
