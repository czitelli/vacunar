﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class VisitaPrestacion
    {
        public int id { get; set; }
        public int visita_id { get; set; }
        public int prestacion_id { get; set; }
        public string prestacion_nombre { get; set; }
    }
}
