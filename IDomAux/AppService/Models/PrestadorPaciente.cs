﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public class PrestadorPaciente
    { 
        public int prestador { get; set; }
        public int paciente { get; set; }
        public int caso { get; set; }
    }
}
