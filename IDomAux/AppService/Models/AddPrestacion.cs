﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Models
{
    public partial class AddPrestacion
    {
        public DateTime created_at { get; set; }
        public string prestacion { get; set; }
        public int paciente_transitorio_id { get; set; }
    }
}
