﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using AppService.Models;
using System.Configuration;
using IDomAux.Core;
using System.Security.Principal;
using System.Web.Security;
using System.Web.Script.Serialization;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using AutoMapper;

namespace AppService.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private Prestador _prestador;
        private IDomAuxCore core;
        private IMapper mapper;

        public ApplicationOAuthProvider(string publicClientId, IDomAuxCore core, IMapper mapper)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }
            this.core = core;
            this.mapper = mapper;

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var prestador = this.core.GetPrestador(context.UserName);
            if (prestador == null)
            {
                context.SetError("invalid_grant", "The user name doesnt exist");
                return;
            }
            else
            {
                var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

                ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                _prestador = mapper.Map<Prestador>(prestador);

                AuthenticationProperties properties = CreateProperties(user.UserName);
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);
                context.Request.Context.Authentication.SignIn(properties, cookiesIdentity);
                //ticket.
                //user.Id = prestador.prestador_id.ToString();
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            context.AdditionalResponseParameters.Add("prestador_id", _prestador.id);
            context.AdditionalResponseParameters.Add("prestador_name", _prestador.name);
            context.AdditionalResponseParameters.Add("prestador_email", _prestador.email);
            context.AdditionalResponseParameters.Add("prestador_alias", _prestador.alias);
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }

        public void CreateAuthenticationTicket(string username)
        {
            //var authUser = Repository.Find(u => u.Username == username);
            //CustomPrincipalSerializedModel serializeModel = new CustomPrincipalSerializedModel();

            //serializeModel.FirstName = authUser.FirstName;
            //serializeModel.LastName = authUser.LastName;
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //string userData = serializer.Serialize(serializeModel);

            //FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
            //  1, username, DateTime.Now, DateTime.Now.AddHours(8), false, userData);
            //string encTicket = FormsAuthentication.Encrypt(authTicket);
            //HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            //HttpContext.Current.Response.Cookies.Add(faCookie);
        }
    }

    public interface ICustomPrincipal : System.Security.Principal.IPrincipal
    {
        string FirstName { get; set; }
        string Email { get; set; }

        string DNI { get; set; }

        int Id { get; set; }
    }

    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }

        public CustomPrincipal(string username)
        {
            this.Identity = new GenericIdentity(username);
        }

        public bool IsInRole(string role)
        {
            return Identity != null && Identity.IsAuthenticated &&
               !string.IsNullOrWhiteSpace(role) && Roles.IsUserInRole(Identity.Name, role);
        }

        public string FirstName { get; set; }

        public string Email { get; set; }

        public string DNI { get; set; }

        public int Id { get; set; }
    }

    public class CustomPrincipalSerializedModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}