﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppService.App_Start;
using AutoMapper;
using IDomAux.Core;
using Microsoft.Owin;
using Owin;

namespace AppService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app, NinjectWebCommon.GetInstance<IDomAuxCore>(), NinjectWebCommon.GetInstance<IMapper>());
        }
    }
}