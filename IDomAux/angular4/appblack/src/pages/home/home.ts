import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PatientsPage } from '../patients/patients';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  patientsRoot = PatientsPage
  constructor(public navCtrl: NavController) {
 
  }
  openPage(PatientsPage) {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.navCtrl.push(PatientsPage);
  }
}
