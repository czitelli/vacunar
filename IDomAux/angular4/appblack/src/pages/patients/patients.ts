import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-patients',
  templateUrl: 'patients.html'
})
export class PatientsPage {

  constructor(public navCtrl: NavController) {

  }

}
