
-- podria agregar una PRIMARY KEY id identity para que la clave no sea compuesta y agregar una unique constraint para los tres campos claves
CREATE TABLE [dbo].[prestador_pacientes](
	[prestador_id] [int] NOT NULL, --
	[caso_id] [int] NOT NULL, -- este campo como lo resuelvo ? dado que a la api solo me postean el paciente_id??
	[paciente_id] [int] NOT NULL,
	[prestacion_id] [int] NOT NULL,
	[prestacion] [varchar](200) NOT NULL,
	CONSTRAINT [PK_prestador_pacientes] PRIMARY KEY CLUSTERED 
	(
		[prestador_id] ASC,
		[caso_id] ASC,
		[prestacion_id] ASC
	)
) ON [PRIMARY]
GO
-- Si mantengo Clave Compuesta la tabla es la siguiente
CREATE TABLE [dbo].[prestador_pacientes_prestaciones](
	[prestador_id] [int] NOT NULL, --identificador otra tabla
	[caso_id] [int] NOT NULL, --identificador otra tabla
	[prestacion_id] [int] NOT NULL, --identificador otra tabla
	[prestador_pacientes_prestaciones_id] [int] Identity (1,1) NOT NULL, 
	[periodicidad_id] [int] NOT NULL,
	[periodicidad] [varchar](100) NOT NULL,
	[fecha_inicio] [datetime] NOT NULL,
	[fecha_fin] [datetime] NULL,
	CONSTRAINT [PK_prestador_pacientes] PRIMARY KEY CLUSTERED 
	(
		prestador_pacientes_prestaciones_id ASC
	)
) ON [PRIMARY]
GO
--Si NO mantengo Clave Compuesta y agrego un identity la tabla es la siguiente
CREATE TABLE [dbo].[prestador_pacientes_prestaciones]( 
	[prestador_pacientes_id] [int] NOT NULL,  --identificador otra tabla
	[prestador_pacientes_prestaciones_id] [int] Identity (1,1) NOT NULL, 
	[periodicidad_id] [int] NOT NULL,
	[periodicidad] [varchar](100) NOT NULL,
	[fecha_inicio] [datetime] NOT NULL,
	[fecha_fin] [datetime] NULL,
	CONSTRAINT [PK_prestador_pacientes] PRIMARY KEY CLUSTERED 
	(
		[prestador_pacientes_prestaciones_id] ASC
	)
) ON [PRIMARY]
GO
