

-- Si mantengo Clave Compuesta la tabla es la siguiente
CREATE TABLE [dbo].[prestador_paciente_prestaciones](
	[prestador_id] [int] NOT NULL, --identificador otra tabla
	[caso_id] [int] NOT NULL, --identificador otra tabla
	[prestacion_id] [int] NOT NULL,
	[periodicidad_id] [int] NOT NULL,
	[periodicidad] [varchar](100) NOT NULL,
	[fecha_inicio] [datetime] NOT NULL,
	[fecha_fin] [datetime] NULL,
	[prestacion] varchar(255) null
	CONSTRAINT [PK_prestador_paciente_prestaciones] PRIMARY KEY CLUSTERED 
	(
		[prestador_id] ASC,
		[caso_id] ASC,
		[prestacion_id] ASC
	)
) ON [PRIMARY]
GO
INSERT [dbo].[prestador_paciente_prestaciones] ([prestador_id], 
	[caso_id],
	[prestacion_id],
	[periodicidad_id],
	[periodicidad],
	[fecha_inicio],
	[fecha_fin])
SELECT [prestador_id], 
	[caso_id],
	[prestacion_id],
	[periodicidad_id],
	[periodicidad],
	[fecha_inicio],
	[fecha_fin]
FROM [prestador_pacientes]
GO
CREATE TABLE [dbo].[prestador_pacientes_2](
	[prestador_id] [int] NOT NULL, --
	[caso_id] [int] NOT NULL, -- este campo como lo resuelvo ? dado que a la api solo me postean el paciente_id?? lo resuelvo apuntando a otra base? 
	[paciente_id] [int] NOT NULL,
	CONSTRAINT [PK_prestador_pacientes_1] PRIMARY KEY CLUSTERED 
	(
		[prestador_id] ASC,
		[caso_id] ASC -- saco un campo clave para llevarlo a la tabla detalles
	)
) ON [PRIMARY]
GO
INSERT [dbo].[prestador_pacientes_2] ([prestador_id], [caso_id], [paciente_id])
SELECT [prestador_id], [caso_id], [paciente_id] 
FROM	(	SELECT [prestador_id], [caso_id], [paciente_id], COUNT(*) cantidad
			FROM [prestador_pacientes]
			GROUP BY [prestador_id], [caso_id], [paciente_id]
		) a
GO
exec sp_rename 'prestador_pacientes', 'prestador_pacientes_old'
exec sp_rename 'prestador_pacientes_2', 'prestador_pacientes'
GO
-- Creo la tabla temporal de paciente transitorio
CREATE TABLE [dbo].[pacientes_transitorios](
	[id] [int] identity(100000000,1) NOT NULL,
	[fecha_alta] [datetime] NOT NULL,
	[prestador_id] [int] NOT NULL,
	[apellido_paciente] [varchar](100) NOT NULL,
	[nombre_paciente] [varchar](100) NOT NULL,
	[dni_paciente] [varchar](10) NOT NULL,
	[direccion_paciente] [varchar](255) NOT NULL
	CONSTRAINT [PK_pacientes_transitorios] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)
) ON [PRIMARY]
GO
-- Creo la tabla temporal de paciente transitorio
CREATE TABLE [dbo].[pacientes_transitorios_prestaciones](
	[id] [int] identity(100000000,1) NOT NULL,
	[fecha_alta] [datetime] NOT NULL,
	[prestacion_brindada] [varchar](255) NOT NULL,
	[paciente_transitorio_id] [int] NOT NULL,
	CONSTRAINT [PK_prestador_pacientes_prestaciones] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)
) ON [PRIMARY]
GO
ALTER Table [dbo].[prestador_visitas] Add [paciente_transitorio_id] int null default NULL

GO
-- Creo la prestadores derivados
CREATE TABLE [dbo].[prestadores_derivados](
	[id] [int] identity(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[prestador_origen_id] [int] NOT NULL,
	[paciente_id] [int] NOT NULL,
	[prestador_destino_id] [int] NOT NULL,
	[motivo_derivacion] [varchar](255) NULL,
	[estado] [varchar](32) NULL, -- pendiente, aprobado, desaprobado
	CONSTRAINT [PK_prestadores_derivados] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)
) ON [PRIMARY]
GO
-- Creo la prestadores derivados
CREATE TABLE [dbo].[paciente_imagenes](
	[id] [int] identity(1,1) NOT NULL,
	[fecha_alta] [datetime] NOT NULL,
	[fecha_imagen] [datetime] NOT NULL,
	[prestador_id] [int] NOT NULL,
	[paciente_id] [int] NOT NULL,
	[prestacion_id] [int] NOT NULL,
	[nombre_imagen] [varchar](255) NULL, -- *.jpg or *.png
	CONSTRAINT [PK_paciente_imagenes] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)
) ON [PRIMARY]
