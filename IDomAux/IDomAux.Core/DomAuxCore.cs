﻿using System;
using System.Data;
using System.Linq;
using IDomAux.Data;
using AppService.Entities;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace IDomAux.Core
{
    public class DomAuxCore : IDomAuxCore
    {
        private readonly IVisitaRepository _visitaRepository;
        private readonly IVisitaObsRepository _visitaObsRepository;
        private readonly IPrestadorPacienteRepository _prestadorPacienteRepository;
        private readonly IPacienteRepository _pacienteRepository;
        private readonly IPrestadorRepository _prestadorRepository;
        private readonly IVisitaPrestacionRepository _visitaPrestacionRepository;
        private readonly IPacientesTransitoriosRepository _pacientesTransitoriosRepository;
        private readonly IPrestadorPacientePrestacionesRepository _prestadorPacientePrestacionesRepository;
        private readonly IPacientesTransitoriosPrestacionesRepository _pacientesTransitoriosPrestacionesRepository;
        private readonly IPrestadorDerivadoRepository _prestadorDerivadoRepository;
        private readonly IPacienteImagenesRepository _pacienteImagenesRepository;

        public DomAuxCore(IVisitaRepository visitaRepository, IPrestadorRepository prestadorRepository,
            IPacienteRepository pacienteRepository, IPrestadorPacienteRepository prestadorPacienteRepository,
            IVisitaObsRepository visitaObsRepository, IVisitaPrestacionRepository visitaPrestacionRepository,
            IPacientesTransitoriosRepository pacientesTransitoriosRepository, 
            IPrestadorPacientePrestacionesRepository prestadorPacientePrestacionesRepository,
            IPacientesTransitoriosPrestacionesRepository pacientesTransitoriosPrestacionesRepository,
            IPrestadorDerivadoRepository prestadorDerivadoRepository,
            IPacienteImagenesRepository pacienteImagenesRepository)
        {
            this._visitaRepository = visitaRepository;
            this._prestadorPacienteRepository = prestadorPacienteRepository;
            this._pacienteRepository = pacienteRepository;
            this._prestadorRepository = prestadorRepository;
            this._visitaObsRepository = visitaObsRepository;
            this._visitaPrestacionRepository = visitaPrestacionRepository;
            this._pacientesTransitoriosRepository = pacientesTransitoriosRepository;
            this._pacientesTransitoriosPrestacionesRepository = pacientesTransitoriosPrestacionesRepository;
            this._prestadorPacientePrestacionesRepository = prestadorPacientePrestacionesRepository;
            this._prestadorDerivadoRepository = prestadorDerivadoRepository;
            this._pacienteImagenesRepository = pacienteImagenesRepository;
        }

        #region Visitas

        public List<prestador_visitas> GetAllVisitas()
        {
            var list = _visitaRepository.GetAll();
            return list.ToList();
        }

        public prestador_paciente_prestaciones GetPrestacion(int prestadorId, int patientId, int prestacionId)
        {
            var paciente = _prestadorPacienteRepository.GetList(i => i.paciente_id == patientId && i.prestador_id == prestadorId).First();
            if (paciente == null)
                return null;
            var tomorrow = DateTime.Today.AddDays(1);
            var prestacion = _prestadorPacientePrestacionesRepository
                       .GetList(i => i.prestador_id == prestadorId
                       && i.prestacion_id == prestacionId
                       && i.caso_id == paciente.caso_id
                       && i.fecha_inicio <= DateTime.Today
                       && (i.fecha_fin.HasValue && i.fecha_fin.Value > tomorrow)).First();
            return prestacion;
        }

        public List<prestador_visitas> GetAllVisits(int profesionalId, int page, int pageSize, out int total, int patientId = 0)
        {
            var list = _visitaRepository.GetList(i => i.prestador_id == profesionalId).ToList();
            total = 0;
            var tomorrow = DateTime.Today.AddDays(1);
            if (patientId != 0)
            {
                var prestaciones = _prestadorPacientePrestacionesRepository
                    .GetList(i => i.prestador_id == profesionalId
                    && i.fecha_inicio <= DateTime.Today
                    && (i.fecha_fin.HasValue && i.fecha_fin.Value > tomorrow))
                    .Select(i => i.prestador_id).ToList();
                var casos = _prestadorPacienteRepository
                    .GetList(i => prestaciones.Contains(i.prestador_id)
                    && i.paciente_id == patientId).Select(e => e.caso_id).ToArray();
                if (!casos.Any())
                {
                    //si el paciente no existe o no esta cubierto, retorno null
                    return null;
                }
                list = list.Where(i => casos.Contains(i.caso_id)).OrderByDescending(v => v.fecha).ToList();
                total = list.Count();

                if (page > 0)
                {
                    list = list.Skip(((page - 1) * pageSize)).Take(pageSize).ToList();
                    var visitsIds = list.Select(i => i.visita_id).ToArray();
                    var prestationVisits = _visitaPrestacionRepository.GetList(x => visitsIds.Contains(x.visita_id))
                        .GroupBy(v => new { v.visita_id }, (v, ps) => new { Key = v, Prestaciones = ps.ToList() });
                    list.ForEach(v =>
                    {
                        var prestacionPorVisita = prestationVisits.FirstOrDefault(i => i.Key.visita_id == v.visita_id);
                        if (prestacionPorVisita != null)
                        {
                            v.prestaciones = prestacionPorVisita.Prestaciones;
                        }
                    });
                }
                else
                {
                }
            }
            return list;
        }

        public List<prestador_visitas> GetAllPendingVisits(int profesionalId, int page, int pageSize, out int total, int patientId)
        {
            total = 0;
            if (patientId == default(int))
                return null;
            var list = _visitaRepository.GetList(i => i.prestador_id == profesionalId && i.paciente_transitorio_id == patientId).ToList();
            list = list.OrderByDescending(v => v.fecha).ToList();
            total = list.Count();
            if (page > 0)
            {
                list = list.Skip(((page - 1) * pageSize)).Take(pageSize).ToList();
                var visitsIds = list.Select(i => i.visita_id).ToArray();
                var prestationVisits = _visitaPrestacionRepository.GetList(x => visitsIds.Contains(x.visita_id))
                    .GroupBy(v => new { v.visita_id }, (v, ps) => new { Key = v, Prestaciones = ps.ToList() });
                list.ForEach(v =>
                {
                    var prestacionPorVisita = prestationVisits.FirstOrDefault(i => i.Key.visita_id == v.visita_id);
                    if (prestacionPorVisita != null)
                    {
                        v.prestaciones = prestacionPorVisita.Prestaciones;
                    }
                });
            }
            else
            {
                return null;
            }
            return list;
            
        }

        public List<prestador_visitas_obs> GetAllObservaciones(List<int> visitasIds)
        {
            return _visitaObsRepository.GetList(i => visitasIds.Contains(i.visita_id)).ToList();
        }

        public pacientes_transitorios_prestaciones GetPrestacion(string pacientePrestacion, int prestadorId)
        {
            return _pacientesTransitoriosPrestacionesRepository.GetList(i => i.prestacion_brindada.Trim().ToUpper() == pacientePrestacion.Trim().ToUpper()).FirstOrDefault();
        }

        public List<prestador_visitas> GetAllVisits(List<int> casos)
        {
            return _visitaRepository.GetList(i => casos.Contains(i.caso_id)).ToList();
        }

        public List<prestador_visitas> GetAllPendingVisits(List<int> pacientes)
        {
            return _visitaRepository.GetList(i => i.paciente_transitorio_id != null && pacientes.Contains(i.paciente_transitorio_id.Value)).ToList();
        }

        public List<prestador_visitas_obs> GetObservaciones(int visitasId)
        {
            return _visitaObsRepository.GetList(i => i.visita_id == visitasId).OrderByDescending(o => o.fecha).ToList();
        }

        public prestador_visitas GetVisit(int visitId, int patientId)
        {
            var result = _visitaRepository.GetList(i => i.visita_id == visitId).FirstOrDefault();
            if (result != null)
            {
                var prestationVisits = _visitaPrestacionRepository.GetList(x => x.visita_id == result.visita_id).ToList();
                if (prestationVisits != null)
                {
                    result.prestaciones = prestationVisits;
                }
            }
            return result;
        }

        public prestador_visitas AddVisit(prestador_visitas visita, int[] prestaciones)
        {
            List<prestador_paciente_prestaciones> prestacionesDelPaciente = new List<prestador_paciente_prestaciones>();
            List<pacientes_transitorios_prestaciones> prestacionesDelPacienteTransitorio = new List<pacientes_transitorios_prestaciones>();
            List<prestador_pacientes> pEntities = new List<prestador_pacientes>();
            pacientes_transitorios pTransitorioEntity = null;
            if (visita.paciente_transitorio_id.HasValue)
            {
                pTransitorioEntity = _pacientesTransitoriosRepository.GetList(i => i.id == visita.paciente_transitorio_id.Value).FirstOrDefault();
                if (pTransitorioEntity != null)
                    prestacionesDelPacienteTransitorio = _pacientesTransitoriosPrestacionesRepository
                        .GetList(i => i.paciente_transitorio_id == pTransitorioEntity.id
                                        && prestaciones.Contains(i.id)
                                        ).ToList();
            }
            else
            {
                var tomorrow = DateTime.Today.AddDays(1);
                var caso = _prestadorPacienteRepository.GetList(p => p.paciente_id == visita.paciente_id && p.prestador_id == visita.prestador_id).FirstOrDefault();
                if (caso == null)
                    return null;
                prestacionesDelPaciente = _prestadorPacientePrestacionesRepository
                    .GetList(i => i.prestador_id == visita.prestador_id && i.caso_id == caso.caso_id
                                  && prestaciones.Contains(i.prestacion_id)
                                  && i.fecha_inicio <= DateTime.Today
                                  && (i.fecha_fin.HasValue && i.fecha_fin.Value > tomorrow))
                    .ToList();
                var prestadoresIds = prestacionesDelPaciente.Select(pp => pp.prestador_id).ToArray();
                pEntities = _prestadorPacienteRepository.GetList(i => i.paciente_id == visita.paciente_id 
                                                                           && prestadoresIds.Contains(i.prestador_id)).ToList();
            }
            if (pEntities.Any() || (visita.paciente_transitorio_id.HasValue && pTransitorioEntity != null && prestacionesDelPacienteTransitorio.Any()))
            {
                if (pEntities.Any())
                {
                    var caso_id = pEntities.Select(i => i.caso_id).FirstOrDefault();
                    visita.caso_id = caso_id;
                }
                var visitaNueva = _visitaRepository.Add(visita);
                visitaNueva.prestaciones = new List<visita_prestacion>();
                if (pEntities.Any())
                {
                    foreach (var prestacion in prestacionesDelPaciente.GroupBy(i => new { i.prestador_id, i.caso_id }))
                    {
                        visitaNueva.prestaciones.Add(AddVisitaPrestacion(visitaNueva, prestacion.First()));
                    }
                }
                else
                {
                    foreach (var prestacionTransitoria in prestacionesDelPacienteTransitorio.GroupBy(p => p.prestacion_brindada))
                    {
                        visitaNueva.prestaciones.Add(AddVisitaPrestacion(visitaNueva, new prestador_paciente_prestaciones()
                        {
                            paciente_id = visita.paciente_transitorio_id.Value,
                            caso_id = 0,
                            prestacion = prestacionTransitoria.Key,
                            prestacion_id = prestacionTransitoria.FirstOrDefault()?.id ?? 0
                        }));
                    }
                }
                return visitaNueva;
            }
            return null;
        }

        public prestador_visitas_obs AddObservacion(prestador_visitas_obs visitaObs)
        {
            return _visitaObsRepository.Add(visitaObs);
        }

        public paciente_imagenes AddImagen(paciente_imagenes imagen)
        {
            imagen.fecha_alta = DateTime.Now;
            return _pacienteImagenesRepository.Add(imagen);
        }

        public paciente_imagenes GetImagen(int id)
        {
            return _pacienteImagenesRepository.GetSingle(i => i.id == id);
        }

        public List<paciente_imagenes> GetImagenesByPatient(int patient_id)
        {
            return _pacienteImagenesRepository.GetList(i => i.paciente_id == patient_id).ToList();
        }

        public List<paciente_imagenes> GetImagenesBy(int prestadorId, int patientId, int prestacionId)
        {
            return _pacienteImagenesRepository.GetList(i => i.paciente_id == patientId && i.prestador_id == prestadorId && i.prestacion_id == prestacionId).ToList();
        }

        public visita_prestacion AddVisitaPrestacion(prestador_visitas visita, prestador_paciente_prestaciones prestacion)
        {
            return _visitaPrestacionRepository.Add(new visita_prestacion
            {
                prestacion_id = prestacion.prestacion_id,
                visita_id = visita.visita_id,
                prestacion_nombre = prestacion.prestacion
            });
        }

        public void EditVisit(prestador_visitas visita)
        {
            _visitaRepository.Update(visita);
        }

        #endregion

        #region Pacientes

        private List<int> GetPacientesPendientesAprobacionSistemaCentral()
        {
            return _prestadorDerivadoRepository.GetList(p => p.estado == EstadoPrestadorDerivado.AprobadoPrestador.ToString()).Select(i => i.paciente_id).ToList();
        }

        private List<int> GetPacientesPendientesDerivacion()
        {
            return _prestadorDerivadoRepository.GetList(p => p.estado == EstadoPrestadorDerivado.Pendiente.ToString()).Select(i => i.paciente_id).ToList();
        }

        public List<pacientes> GetAllPatients(int prestador, out int totalItems, int? page = 1, int? pageSize = 10)
        {
            totalItems = 0;
            var tomorrow = DateTime.Today.AddDays(1);
            var prestaciones = _prestadorPacientePrestacionesRepository.GetList(i => i.prestador_id == prestador
                                                                                     && i.fecha_inicio <= DateTime.Today
                                                                                     && (i.fecha_fin.HasValue &&
                                                                                         i.fecha_fin.Value > tomorrow))
                .ToList();
            var prestacionesPrestadorIds = prestaciones.Select(i => i.prestador_id).ToList();
            var pacientesPendientesDeConfirmacionPrestador = GetPacientesPendientesDerivacion();
            var pacientesPendientesDeConfirmacionSistema = GetPacientesPendientesAprobacionSistemaCentral();
            var patientsCases = _prestadorPacienteRepository.GetList(i => prestacionesPrestadorIds.Contains(i.prestador_id) && !pacientesPendientesDeConfirmacionPrestador.Contains(i.paciente_id)).ToList();
            var patientsIds = patientsCases.Select(e => e.paciente_id).ToArray();
            var casesIds = patientsCases.Select(e => e.caso_id).ToList();
            var listPatients = _pacienteRepository.GetList(i => patientsIds.Contains(i.paciente_id)).ToList();
            var groupByCase = prestaciones.GroupBy(x => new {x.caso_id, x.prestador_id }, (r, t) => new { Key = r, Prestaciones = t.ToList() });
            var patientsVisits = GetAllVisits(casesIds).ToList();
            var groupByVisits = patientsVisits.GroupBy(x => new { x.paciente_id, x.prestador_id }, (r, t) => new { Key = r, Visits = t.ToList() });
            listPatients.ForEach(i =>
            {
                var currentPatientCase =  patientsCases.FirstOrDefault(p => p.paciente_id == i.paciente_id);
                var cases = groupByCase.FirstOrDefault(e => e.Key.caso_id == currentPatientCase.caso_id);
                if (cases != null)
                {
                    i.prestaciones = cases.Prestaciones;
                    
                    foreach (var prestacion in i.prestaciones)
                    {
                        prestacion.paciente_id = i.paciente_id;
                        prestacion.imagenes = this.GetImagenesBy(prestador, prestacion.paciente_id, prestacion.prestacion_id);
                    }
                    i.caso_id = cases.Key.caso_id;
                }
                if (pacientesPendientesDeConfirmacionSistema.Contains(i.paciente_id))
                    i.posible_derivacion = 0;
                else
                    i.posible_derivacion = 1;
                var visits = groupByVisits.FirstOrDefault(e => e.Key.paciente_id == i.paciente_id);
                if (visits != null)
                    i.visitas = visits.Visits;
            });
            totalItems = listPatients.Count();
            //listPatients = listPatients.Skip(((page.Value - 1) * pageSize.Value)).Take(pageSize.Value).ToList();
            return listPatients;
        }
        public List<pacientes_transitorios> GetAllPendingPatients(int prestador, out int totalItems, int? page = 1, int? pageSize = 10)
        {
            totalItems = 0;
            var tomorrow = DateTime.Today.AddDays(1);
            var patientsCases = _pacientesTransitoriosRepository.GetList(i => i.prestador_id == prestador )
                                                                .ToList();
            patientsCases = excludeApprovedCases(patientsCases, prestador);
            var patientsIds = patientsCases.Select(e => e.id).ToList();
            var patientsVisits = GetAllPendingVisits(patientsIds).ToList();
            var groupByVisits = patientsVisits.GroupBy(x => new { x.paciente_transitorio_id, x.prestador_id }, (r, t) => new { Key = r, Visits = t.ToList() });
            patientsCases.ForEach(i =>
            {
                i.prestaciones = _pacientesTransitoriosPrestacionesRepository
                                        .GetList(p => p.paciente_transitorio_id == i.id 
                                                    && p.prestacion_brindada.Trim().ToUpper() == i.nombre_paciente.Trim().ToUpper())
                                                    .Select(pp => new prestador_paciente_prestaciones()
                                                    {
                                                        fecha_inicio = pp.fecha_alta,
                                                        prestacion = pp.prestacion_brindada,
                                                        prestador_id = i.prestador_id,
                                                        prestacion_id = pp.id
                                                    }).ToList();
                var visits = groupByVisits.FirstOrDefault(e => e.Key.paciente_transitorio_id == i.id);
                if (visits != null)
                    i.visitas = visits.Visits;
            });
            totalItems = patientsCases.Count();
            //patientsCases = patientsCases.Skip(((page.Value - 1) * pageSize.Value)).Take(pageSize.Value).ToList();
            return patientsCases;
        }

        public pacientes_transitorios GetPendingPatient(int patientId, int prestador)
        {
            var tomorrow = DateTime.Today.AddDays(1);
            var patientCase = _pacientesTransitoriosRepository.GetList(i => i.prestador_id == prestador && i.id == patientId)
                                                                .FirstOrDefault();
            var patientVisits = _visitaRepository.GetList(i => i .prestador_id == prestador 
            && i.paciente_transitorio_id.HasValue && i.paciente_transitorio_id == patientId)
            .ToList();
            patientCase.prestaciones = _pacientesTransitoriosPrestacionesRepository
                                        .GetList(p => p.paciente_transitorio_id == patientCase.id)
                                                    .Select(pp => new prestador_paciente_prestaciones()
                                                    {
                                                        fecha_inicio = pp.fecha_alta,
                                                        prestacion = pp.prestacion_brindada,
                                                        prestador_id = patientCase.prestador_id,
                                                        prestacion_id = pp.id
                                                    }).ToList();
            var visitsIds = patientVisits.Select(v => v.visita_id);
            var prestationVisits = _visitaPrestacionRepository.GetList(x => visitsIds.Contains(x.visita_id))
                .GroupBy(v => new { v.visita_id }, (v, ps) => new { Key = v, Prestaciones = ps.ToList() });
            patientVisits.ForEach(v =>
            {
                var prestacionPorVisita = prestationVisits.FirstOrDefault(i => i.Key.visita_id == v.visita_id);
                if (prestacionPorVisita != null)
                {
                    v.prestaciones = prestacionPorVisita.Prestaciones;
                }
            });
            patientCase.visitas = patientVisits.OrderByDescending(v => v.fecha).ToList();
            return patientCase;
        }

        private List<pacientes_transitorios> excludeApprovedCases(List<pacientes_transitorios> pacientes, int prestador)
        {
            var tomorrow = DateTime.Today.AddDays(1);
            var prestaciones = _prestadorPacientePrestacionesRepository.GetList(i =>
                i.prestador_id == prestador && i.fecha_inicio <= DateTime.Today
                && (i.fecha_fin.HasValue &&
                    i.fecha_fin.Value > tomorrow)).ToList();
            var casosIds = prestaciones.Select(i => i.caso_id).Distinct();
            
            var approvedPatients = _prestadorPacienteRepository
                .GetList(i => casosIds.Contains(i.caso_id) && i.prestador_id == prestador).ToList();
            var approvedPatientsIds = approvedPatients.Select(i => i.paciente_id).Distinct();
            var listPatients = _pacienteRepository.GetList(i => approvedPatientsIds.Contains(i.paciente_id)).ToList();
            prestaciones.ForEach(p =>
            {
                p.paciente_id = approvedPatients.First(i => i.caso_id == p.caso_id).paciente_id;
                p.dni = listPatients.First(pp => pp.paciente_id == p.paciente_id).dni;
            });
            var prestacionesPorDni = prestaciones.Join(pacientes, pacientePrestaciones => new prestador_paciente_prestaciones()
            {
                dni = pacientePrestaciones.dni
            }, transitorios => new prestador_paciente_prestaciones
            {
                dni = transitorios.dni_paciente
            }, (pacientePrestaciones, transitorios) =>  new { transitorios, pacientePrestaciones });
            var pacientesNoAprobados = prestacionesPorDni.Where(i => i.transitorios.fecha_alta.Date > i.pacientePrestaciones.fecha_inicio.Date).Select(ppp => ppp.transitorios.dni_paciente).ToList();
            return pacientes.Where(i => !pacientesNoAprobados.Contains(i.dni_paciente)).ToList();
        }

        public pacientes GetPatient(int patientId, int prestadorId)
        {
            var tomorrow = DateTime.Today.AddDays(1);
            var prestaciones = _prestadorPacientePrestacionesRepository.GetList(i => i.prestador_id == prestadorId
                                                                                     && i.fecha_inicio <= DateTime.Today
                                                                                     && (i.fecha_fin.HasValue &&
                                                                                         i.fecha_fin.Value > tomorrow))
                .ToList();
            var patientsCase = _prestadorPacienteRepository.GetList(i => i.prestador_id == prestadorId && i.paciente_id == patientId).FirstOrDefault();
            pacientes patient = null;
            List<prestador_paciente_prestaciones> prestacionesPaciente = null;
            List<prestador_visitas> patientVisits = null;
            if (patientsCase != null)
            {
                patient = _pacienteRepository.GetList(i => patientId == i.paciente_id).FirstOrDefault();
                prestacionesPaciente = prestaciones.Where(i => i.caso_id == patientsCase.caso_id).ToList();
                patientVisits = GetAllVisits(prestacionesPaciente.Select(i => i.caso_id).ToList()).ToList();
            }

            if (patient != null)
            {
                patient.caso_id = patientsCase.caso_id;
                patient.prestaciones = prestacionesPaciente;
                if (patient.prestaciones.Any())
                {
                    var visitsIds = patientVisits.Select(i => i.visita_id).ToArray();
                    var prestationVisits = _visitaPrestacionRepository.GetList(x => visitsIds.Contains(x.visita_id))
                        .GroupBy(v => new { v.visita_id }, (v, ps) => new { Key = v, Prestaciones = ps.ToList() });
                    patientVisits.ForEach(v =>
                    {
                        var prestacionPorVisita = prestationVisits.FirstOrDefault(i => i.Key.visita_id == v.visita_id);
                        if (prestacionPorVisita != null)
                        {
                            v.prestaciones = prestacionPorVisita.Prestaciones;
                        }
                    });
                }
                patient.visitas = patientVisits.OrderByDescending(i => i.fecha).ToList();
            }
            return patient;
        }

        #endregion

        #region Prestadores

        public prestadores GetPrestador(string email)
        {
            var list = _prestadorRepository.GetList(i => i.email.ToUpper() == email.ToUpper());
            return list.FirstOrDefault();
        }

        public pacientes_transitorios AddPatient(pacientes_transitorios paciente, int prestadorId, string prestacion)
        {
            var patient = _pacientesTransitoriosRepository.Add(paciente);
            var prestacionEntity = new pacientes_transitorios_prestaciones
            {
                fecha_alta = DateTime.Now,
                paciente_transitorio_id = patient.id,
                prestacion_brindada = prestacion
            };
            var prestacionSaved = AddPrestacion(prestacionEntity, prestadorId);
            patient.prestaciones = new List<prestador_paciente_prestaciones>();
            patient.prestaciones.Add(new prestador_paciente_prestaciones()
            {
                fecha_inicio = prestacionSaved.fecha_alta,
                prestacion = prestacionSaved.prestacion_brindada,
                prestador_id = prestadorId,
                prestacion_id = prestacionSaved.id
            });
            return patient;
        }

        public prestadores_derivados GetDerivacion(int prestadorId, int derivacionId)
        {
            return _prestadorDerivadoRepository.GetList(p => p.prestador_destino_id == prestadorId && p.id == derivacionId).First();
        }

        public void AceptarDerivarPaciente(prestadores_derivados derivacion)
        {
            var prestadorPacienteAntiguos = _prestadorPacienteRepository.GetList(i => i.paciente_id == derivacion.paciente_id && i.prestador_id == derivacion.prestador_origen_id).ToList();
            if (prestadorPacienteAntiguos.Any())
            {
                prestadorPacienteAntiguos.ForEach(p =>
                {
                    _prestadorPacienteRepository.Remove(p);
                    p.prestador_id = derivacion.prestador_destino_id;
                    _prestadorPacienteRepository.Add(p);
                    var prestacionesAntiguas = _prestadorPacientePrestacionesRepository.GetList(i => i.caso_id == p.caso_id && i.prestador_id == derivacion.prestador_origen_id).ToList();
                    if (prestacionesAntiguas.Any())
                    {
                        prestacionesAntiguas.ForEach(ppp =>
                        {
                            _prestadorPacientePrestacionesRepository.Remove(ppp);
                            ppp.prestador_id = derivacion.prestador_destino_id;
                            _prestadorPacientePrestacionesRepository.Add(ppp);
                        });
                    }
                });
            }
        }

        public prestadores_derivados ActualizarEstadoDerivacion(int prestadorId, int derivacionId, string estado)
        {
            var prestacion = _prestadorDerivadoRepository.GetList(i => i.prestador_destino_id == prestadorId && i.id == derivacionId).FirstOrDefault();
            if (prestacion == null)
                return null;
            prestacion.estado = estado;
            _prestadorDerivadoRepository.Update(prestacion);
            return prestacion;
        }

        public prestadores_derivados AddPrestadorDerivado(prestadores_derivados prestador, int prestadorId, out string message)
        {
            message = "Ok";
            prestador.prestador_origen_id = prestadorId;
            if (prestador.prestador_destino_id == prestadorId)
            {
                message = "el prestador derivado debe ser diferente al prestador actual";
                return null;
            }
            if (_prestadorDerivadoRepository.GetList(i => i.prestador_destino_id == prestador.prestador_destino_id && i.prestador_origen_id == prestador.prestador_origen_id && i.paciente_id == prestador.paciente_id).Any())
            {
                message = "el paciente ya esta derivado al prestador";
                return null;
            }
            else
                if (_prestadorDerivadoRepository.GetList(i => i.paciente_id == prestador.paciente_id && (i.estado == EstadoPrestadorDerivado.Pendiente.ToString() || i.estado == EstadoPrestadorDerivado.AprobadoPrestador.ToString())).Any())
                {
                    message = "el paciente ya tiene una derivación pendiente (sistema o prestador)";
                    return null;
                }
            if (_prestadorPacienteRepository.GetList(i => i.paciente_id == prestador.paciente_id && i.prestador_id == prestador.prestador_destino_id).Any())
            {
                message = "el paciente ya le pertenece al prestador";
                return null;
            }
            prestador.fecha = DateTime.Now;
            return _prestadorDerivadoRepository.Add(prestador);
        }

        public pacientes_transitorios_prestaciones AddPrestacion(pacientes_transitorios_prestaciones pacientePrestacion, int prestadorId)
        {
            var patient = _pacientesTransitoriosPrestacionesRepository.Add(pacientePrestacion);
            return patient;
        }

        public pacientes_transitorios GetPatientByDni(string dni)
        {
            var list = _pacientesTransitoriosRepository.GetList(i => i.dni_paciente.ToUpper() == dni.ToUpper());
            return list.FirstOrDefault();
        }

        public List<prestadores> GetAllPrestadores(int providerId, out int totalItems)
        {
            var list = _prestadorRepository.GetList(i => i.prestador_id != providerId);
            totalItems = list.Count();
            return list.ToList();
        }

        public List<prestadores_derivados> GetPrestadoresDerivados(int prestadorId)
        {
            var pacientes = _pacienteRepository.GetAll();
            var listEmisor = _prestadorDerivadoRepository.GetList(pd => pd.prestador_origen_id == prestadorId)
                .Select(p => 
                    {
                        p.tipo_derivacion = TipoDerivacion.emisor.ToString();
                        p.paciente = pacientes.FirstOrDefault(i => i.paciente_id == p.paciente_id);
                        return p;
                    }).ToList();
            listEmisor.AddRange(_prestadorDerivadoRepository.GetList(pd => pd.prestador_destino_id == prestadorId)
                .Select(p =>
                {
                    p.tipo_derivacion = TipoDerivacion.receptor.ToString();
                    p.paciente = pacientes.FirstOrDefault(i => i.paciente_id == p.paciente_id);
                    return p;
                }));
            return listEmisor;
        }

        #endregion
    }
}