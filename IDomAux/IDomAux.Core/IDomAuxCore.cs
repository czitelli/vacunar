﻿using System;
using System.Data;
using AppService.Entities;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace IDomAux.Core
{
    public interface IDomAuxCore
    {
        List<prestador_visitas> GetAllVisitas();
        List<prestador_visitas> GetAllVisits(List<int> visitasIds);
        List<prestador_visitas> GetAllPendingVisits(List<int> pacientes);
        List<prestador_visitas> GetAllPendingVisits(int profesionalId, int page, int pageSize, out int total, int patientId);
        List<prestador_visitas> GetAllVisits(int profesionalId, int page, int pageSize, out int total, int patientId = 0);
        prestador_visitas GetVisit(int visitId, int patientId);
        List<pacientes> GetAllPatients(int prestador, out int totalItems, int? page = 1, int? pageSize = 10);
        List<pacientes_transitorios> GetAllPendingPatients(int prestador, out int totalItems, int? page = 1, int? pageSize = 10);
        List<prestadores> GetAllPrestadores(int providerId, out int totalItems);
        pacientes GetPatient(int patientId, int prestadorId);
        prestador_paciente_prestaciones GetPrestacion(int prestadorId, int patientId, int prestacionId);
        paciente_imagenes GetImagen(int id);
        List<paciente_imagenes> GetImagenesByPatient(int patient_id);
        List<paciente_imagenes> GetImagenesBy(int prestadorId, int patientId, int prestacionId);
        paciente_imagenes AddImagen(paciente_imagenes imagen);
        pacientes_transitorios_prestaciones AddPrestacion(pacientes_transitorios_prestaciones pacientePrestacion, int prestadorId);
        pacientes_transitorios_prestaciones GetPrestacion(string pacientePrestacion, int prestadorId);
        pacientes_transitorios GetPendingPatient(int patientId, int prestador);
        prestadores_derivados GetDerivacion(int patientId, int derivacionId);
        pacientes_transitorios GetPatientByDni(string dni);
        prestadores GetPrestador(string email);
        prestador_visitas_obs AddObservacion(prestador_visitas_obs visitaObs);
        prestadores_derivados AddPrestadorDerivado(prestadores_derivados prestador, int prestadorId, out string message);
        prestadores_derivados ActualizarEstadoDerivacion(int prestadorId, int derivacionId, string estado);
        prestador_visitas AddVisit(prestador_visitas visita, int[] prestaciones);
        List<prestador_visitas_obs> GetAllObservaciones(List<int> visitasIds);
        List<prestador_visitas_obs> GetObservaciones(int visitasId);
        void AceptarDerivarPaciente(prestadores_derivados derivacion);
        List<prestadores_derivados> GetPrestadoresDerivados(int prestadorId);
        pacientes_transitorios AddPatient(pacientes_transitorios paciente, int prestadorId, string prestacion);

    }
}
