//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppService.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class prestador_paciente_prestaciones
    {
        [Key, ColumnAttribute(Order = 0)]
        public int prestador_id { get; set; }
        [Key, ColumnAttribute(Order = 1)]
        public int caso_id { get; set; }
        [Key, ColumnAttribute(Order = 2)]
        public int prestacion_id { get; set; }
        public int periodicidad_id { get; set; }
        public string periodicidad { get; set; }
        public System.DateTime fecha_inicio { get; set; }
        public Nullable<System.DateTime> fecha_fin { get; set; }
        public string prestacion { get; set; }
    }
}
