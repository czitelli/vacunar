using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace AppService.Entities
{

    public partial class paciente_imagenes
    {
        [NotMapped]
        private string baseFolder { get { return "/prestador/" + this.prestador_id + "/paciente/" + this.paciente_id + "/prestacion/" + this.prestacion_id + "/imagenes/" + this.fecha_imagen.Value.ToString("yyyy-MM-ddTHH-mm-ss"); } }

        [NotMapped]
        public string folder { get { return "~" + this.baseFolder; } }

        [NotMapped]
        public string url_imagen { get { return this.baseFolder + "/" + this.nombre_imagen; } }

    }
}
