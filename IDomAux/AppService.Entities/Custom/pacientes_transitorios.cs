using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace AppService.Entities
{

    public partial class pacientes_transitorios
    {
        [NotMapped]
        public IList<prestador_paciente_prestaciones> prestaciones { get; set; }
        [NotMapped]
        public IList<prestador_visitas> visitas { get; set; }

    }
}
