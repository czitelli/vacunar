using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppService.Entities
{

    public partial class prestador_visitas
    {
        //[Key]
        //public int visita_id_ { get; set; }
        [NotMapped]
        public int paciente_id { get; set; }
        [NotMapped]
        public IList<visita_prestacion> prestaciones { get; set; }

    }
}
