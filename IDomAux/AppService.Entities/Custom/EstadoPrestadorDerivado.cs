using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AppService.Entities
{
    public enum EstadoPrestadorDerivado
    {
        Pendiente, AprobadoPrestador, AprobadoSistema, Cancelado
    }
}
