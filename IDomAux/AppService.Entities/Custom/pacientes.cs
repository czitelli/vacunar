using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace AppService.Entities
{

    public partial class pacientes
    {
        //[Key]  add this DataAnnotations
        //public int paciente_id { get; set; }
        [NotMapped]
        public string frecuencia { get; set; }
        [NotMapped]
        public IList<prestador_paciente_prestaciones> prestaciones { get; set; }
        [NotMapped]
        public IList<prestador_visitas> visitas { get; set; }
        [NotMapped]
        public int caso_id { get; set; }
        [NotMapped]
        public int posible_derivacion { get; set; }

    }
}
