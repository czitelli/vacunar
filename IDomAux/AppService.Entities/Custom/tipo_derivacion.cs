using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AppService.Entities
{
    public enum TipoDerivacion
    {
        receptor, emisor
    }
}
