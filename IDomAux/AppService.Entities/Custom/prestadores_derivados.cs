using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace AppService.Entities
{

    public partial class prestadores_derivados
    {
        [NotMapped]
        public string tipo_derivacion { get; set; }
        [NotMapped]
        public pacientes paciente { get; set; }
        [NotMapped]
        public bool es_emisor { get { return tipo_derivacion == TipoDerivacion.emisor.ToString(); } }
    }
}
