
namespace AppService.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class prestador_paciente_prestaciones
    {
        [NotMapped]
        public int paciente_id { get; set; }
        [NotMapped]
        public string dni { get; set; }
        [NotMapped]
        public IList<paciente_imagenes> imagenes { get; set; }
    }
}
