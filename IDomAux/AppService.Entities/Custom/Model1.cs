namespace AppService.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public partial class AppServiceEntities : DbContext
    {
        public AppServiceEntities(string conn)
            : base(conn)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
            //Database.SetInitializer<AppServiceEntities>(new CreateDatabaseIfNotExists<AppServiceEntities>());
        }
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    //TODO: add configurations with fluent api
        //}
    }
}