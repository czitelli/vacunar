//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppService.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class pacientes_transitorios
    {
        [Key]
        public int id { get; set; }
        public System.DateTime fecha_alta { get; set; }
        public int prestador_id { get; set; }
        public string apellido_paciente { get; set; }
        public string nombre_paciente { get; set; }
        public string dni_paciente { get; set; }
        public string direccion_paciente { get; set; }
    }
}
