
/*
	Proyecto Internacion Domiciliaria
	
	Script: tablas de archivos auxiliares
	
		- id_periodicidad
			: definicion de periodicidad de eventos
			
		.- id_zonas
			: zonas geograficas
	
		.- id_insumo_grupos
			: definicion de grupos de insumos
		.- id_insumo_x_grupo
			: lista de insumos por grupo
			
		.- id_especialidades
			: descripcion de servicio brindado
		.- id_especialidad_tarifas
			: costos de los servicios brindados por zonas

		.- id_practicas
			: modulo de servicio
		.- id_practica_prestaciones
			: lista de especialidad/periodicidad incluidas en la practica
		.- id_practica_insumos
			: lista de insumos incluidos en la practica
		.- id_practica_relacion
			: relacion entre codigos de practica propios y codigos de practica de obra social (modulo)
		.- id_practica_tarifas
			: costos de los modulos por obra social
*/

/****** Object:  Table [dbo].[id_periodicidad] ******/
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_practica_prestaciones') AND name = 'FK_pp_periodicidad')
	ALTER TABLE [dbo].[id_practica_prestaciones] DROP CONSTRAINT [FK_pp_periodicidad] 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_periodicidad]') AND type in (N'U'))
	DROP TABLE [dbo].[id_periodicidad]
GO

CREATE TABLE [dbo].[id_periodicidad](
	[periodicidad_id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](20) NOT NULL,
	[periodo] [int] NOT NULL,
	[ocurrencias] [int] NOT NULL,
	CONSTRAINT [PK_id_periodicidad] PRIMARY KEY CLUSTERED 
	(
		[periodicidad_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_zonas] ******/
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_especialidad_tarifas') AND name = 'FK_et_zona')
	ALTER TABLE [dbo].[id_especialidad_tarifas] DROP CONSTRAINT [FK_et_zona] 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_zonas]') AND type in (N'U'))
	DROP TABLE [dbo].[id_zonas]
GO

CREATE TABLE [dbo].[id_zonas](
	[zona_id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	CONSTRAINT [PK_id_zonas] PRIMARY KEY CLUSTERED 
	(
		[zona_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_insumo_grupos] ******/
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_insumo_x_grupo') AND name = 'FK_ixg_grupo')
	ALTER TABLE [dbo].[id_insumo_x_grupo] DROP CONSTRAINT [FK_ixg_grupo] 
GO
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_practica_insumos') AND name = 'FK_pi_grupo')
	ALTER TABLE [dbo].[id_practica_insumos] DROP CONSTRAINT [FK_pi_grupo] 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_insumo_grupos]') AND type in (N'U'))
	DROP TABLE [dbo].[id_insumo_grupos]
GO

CREATE TABLE [dbo].[id_insumo_grupos](
	[grupo_id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	CONSTRAINT [PK_id_insumo_grupos] PRIMARY KEY CLUSTERED 
	(
		[grupo_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_insumo_x_grupo] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_insumo_x_grupo]') AND type in (N'U'))
	DROP TABLE [dbo].[id_insumo_x_grupo]
GO

CREATE TABLE [dbo].[id_insumo_x_grupo](
	[ixg_id] [int] IDENTITY(1,1) NOT NULL,
	[grupo_id] [int] NOT NULL CONSTRAINT FK_ixg_grupo FOREIGN KEY REFERENCES id_insumo_grupos(grupo_id),
	[art_codigo] [char](15) NOT NULL,
	CONSTRAINT [PK_id_insumo_x_grupo] PRIMARY KEY CLUSTERED 
	(
		[ixg_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_especialidades] ******/
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_especialidad_tarifas') AND name = 'FK_et_especialidad')
	ALTER TABLE [dbo].[id_especialidad_tarifas] DROP CONSTRAINT [FK_et_especialidad] 
GO
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_practica_prestaciones') AND name = 'FK_pp_especialidad')
	ALTER TABLE [dbo].[id_practica_prestaciones] DROP CONSTRAINT [FK_pp_especialidad] 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_especialidades]') AND type in (N'U'))
	DROP TABLE [dbo].[id_especialidades]
GO

CREATE TABLE [dbo].[id_especialidades](
	[especialidad_id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	CONSTRAINT [PK_id_especialidades] PRIMARY KEY CLUSTERED 
	(
		[especialidad_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_especialidad_tarifas] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_especialidad_tarifas]') AND type in (N'U'))
	DROP TABLE [dbo].[id_especialidad_tarifas]
GO

CREATE TABLE [dbo].[id_especialidad_tarifas](
	[esptar_id] [int] IDENTITY(1,1) NOT NULL,
	[especialidad_id] [int] NOT NULL CONSTRAINT FK_et_especialidad FOREIGN KEY REFERENCES id_especialidades(especialidad_id), 
	[zona_id] [int] NOT NULL CONSTRAINT FK_et_zona FOREIGN KEY REFERENCES id_zonas(zona_id), 
	[vigencia_desde] [datetime] NOT NULL,
	[vigencia_hasta] [datetime] NOT NULL,
	[costo] [numeric](10, 3) NOT NULL,
	CONSTRAINT [PK_id_especialidad_tarifas] PRIMARY KEY CLUSTERED 
	(
		[esptar_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_practicas] ******/
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_practica_prestaciones') AND name = 'FK_pp_practica')
	ALTER TABLE [dbo].[id_practica_prestaciones] DROP CONSTRAINT [FK_pp_practica] 
GO
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_practica_insumos') AND name = 'FK_pi_practica')
	ALTER TABLE [dbo].[id_practica_insumos] DROP CONSTRAINT [FK_pi_practica] 
GO
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_practica_relacion') AND name = 'FK_pr_practica')
	ALTER TABLE [dbo].[id_practica_relacion] DROP CONSTRAINT [FK_pr_practica] 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_practicas]') AND type in (N'U'))
	DROP TABLE [dbo].[id_practicas]
GO

CREATE TABLE [dbo].[id_practicas](
	[practica_id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
	[es_ciclica] [int] NOT NULL,
	CONSTRAINT [PK_id_practicas] PRIMARY KEY CLUSTERED 
	(
		[practica_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_practica_prestaciones] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_practica_prestaciones]') AND type in (N'U'))
	DROP TABLE [dbo].[id_practica_prestaciones]
GO

CREATE TABLE [dbo].[id_practica_prestaciones](
	[prestacion_id] [int] IDENTITY(1,1) NOT NULL,
	[practica_id] [int] NOT NULL CONSTRAINT FK_pp_practica FOREIGN KEY REFERENCES id_practicas(practica_id),
	[especialidad_id] [int] NOT NULL CONSTRAINT FK_pp_especialidad FOREIGN KEY REFERENCES id_especialidades(especialidad_id),
	[periodicidad_id] [int] NOT NULL CONSTRAINT FK_pp_periodicidad FOREIGN KEY REFERENCES id_periodicidad(periodicidad_id),
	CONSTRAINT [PK_id_practica_prestaciones] PRIMARY KEY CLUSTERED 
	(
		[prestacion_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_practica_insumos] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_practica_insumos]') AND type in (N'U'))
	DROP TABLE [dbo].[id_practica_insumos]
GO

CREATE TABLE [dbo].[id_practica_insumos](
	[insumo_id] [int] IDENTITY(1,1) NOT NULL,
	[practica_id] [int] NOT NULL CONSTRAINT FK_pi_practica FOREIGN KEY REFERENCES id_practicas(practica_id),
	[grupo_id] [int] NOT NULL CONSTRAINT FK_pi_grupo FOREIGN KEY REFERENCES id_insumo_grupos(grupo_id),
	CONSTRAINT [PK_id_practica_insumos] PRIMARY KEY CLUSTERED 
	(
		[insumo_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_practica_relacion] ******/
IF EXISTS(SELECT * FROM sys.objects WHERE parent_object_id = OBJECT_ID('id_practica_tarifas') AND name = 'FK_pt_relacion')
	ALTER TABLE [dbo].[id_practica_tarifas] DROP CONSTRAINT [FK_pt_relacion] 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_practica_relacion]') AND type in (N'U'))
	DROP TABLE [dbo].[id_practica_relacion]
GO

CREATE TABLE [dbo].[id_practica_relacion](
	[relacion_id] [int] IDENTITY(1,1) NOT NULL,
	[practica_id] [int] NOT NULL CONSTRAINT FK_pr_practica FOREIGN KEY REFERENCES id_practicas(practica_id),
	[obrasocial_id] [int] NOT NULL,
	[modulo] [varchar](30) NOT NULL,
	CONSTRAINT [PK_id_practica_relacion] PRIMARY KEY CLUSTERED 
	(
		[relacion_id] ASC
	)
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[id_practica_tarifas] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[id_practica_tarifas]') AND type in (N'U'))
	DROP TABLE [dbo].[id_practica_tarifas]
GO

CREATE TABLE [dbo].[id_practica_tarifas](
	[tarifa_id] [int] IDENTITY(1,1) NOT NULL,
	[relacion_id] [int] NOT NULL CONSTRAINT FK_pt_relacion FOREIGN KEY REFERENCES id_practica_relacion(relacion_id),
	[vigencia_desde] [datetime] NOT NULL,
	[vigencia_hasta] [datetime] NOT NULL,
	[costo] [numeric](10, 3) NOT NULL,
	CONSTRAINT [PK_id_practica_tarifas] PRIMARY KEY CLUSTERED 
	(
		[tarifa_id] ASC
	)
) ON [PRIMARY]

GO
